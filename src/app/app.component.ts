import { Component, AfterViewInit } from '@angular/core';
import { ViewportScroller } from '@angular/common';
import { Router, Scroll, NavigationEnd, ActivatedRoute } from '@angular/router';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements AfterViewInit {
  title = 'company-develop-intro';
  doc = document;

  currentItem = "webdevelop";
  items = [ "webdevelop", "appdev", "uxdesign" ];
  currentItemIndex = null;

  constructor(private viewportScroller: ViewportScroller, private router: Router, private route: ActivatedRoute) {
    /*this.route.fragment.subscribe(f => {
      if(f) {
        const element = document.querySelector("#" + f)
        if (element) element.scrollIntoView()
      }
    })
    */

    /*this.router.events.pipe(filter(e => e instanceof Scroll)).subscribe((e: any) => {
      console.log(e);
      if(!(e.anchor)) {
        return;
      }
      // this is fix for dynamic generated(loaded..?) content
      setTimeout(() => {
        if (e.position) {
          this.viewportScroller.scrollToPosition(e.position);
        } else if (e.anchor) {
            const element = document.getElementById( e.anchor); // document.querySelector("#" + e.anchor)
            if (element) element.scrollIntoView({behavior: "smooth", block: "start", inline: "nearest"})
          // this.viewportScroller.scrollToAnchor(e.anchor);
        } else {
          this.viewportScroller.scrollToPosition([0, 0]);
        }
      });
    });*/
  }

  ngAfterViewInit(): void {
    this.route.fragment.subscribe(f => {
      if(f) {
        setTimeout(() => {
          this.ButtonOnClick(f);
        });
      }
    })
  }

  public ButtonOnClick(elementId: string): any { 
    //evt.preventDefault();
    //this.router.navigate(['/'],{fragment: 'contact'});
    //this.viewportScroller.scrollToAnchor(elementId);
    const element = document.getElementById( elementId); // document.querySelector("#" + e.anchor)
    if (element) element.scrollIntoView({behavior: "smooth", block: "start", inline: "nearest"})
 
  }

  openDetails(num: number) {
    if(this.currentItemIndex !== num) {   

      const backupIndex = this.currentItemIndex;
      
      if(this.doc.getElementsByClassName('leftRightArrow')[0].classList.contains('hidden')) {
        this.doc.getElementsByClassName('leftRightArrow')[0].classList.remove('hidden'); 
        this.doc.getElementsByClassName('leftRightArrow')[1].classList.remove('hidden'); 
      } else {
        this.doc.getElementsByClassName('leftRightArrow')[0].classList.add('hidden'); 
        this.doc.getElementsByClassName('leftRightArrow')[1].classList.add('hidden'); 
        
      }
      this.currentItemIndex = num;

      for(let i=0; i<3; i++) {
        if(i!==num) {
          this.doc.getElementsByClassName('service-type')[num].classList.remove('active'); 
        }
      }
      this.doc.getElementsByClassName('service-type')[num].classList.add('active'); 
      
      if(this.doc.getElementsByClassName('services-container')[0].classList.contains('hidden')===false) {
        this.doc.getElementsByClassName('services-container')[0].classList.add('hidden');
      }
      this.doc.getElementsByClassName('test')[this.currentItemIndex].classList.add("active");
    
      if(backupIndex !== null && backupIndex !== num) {
        this.doc.getElementsByClassName('test')[backupIndex].classList.add("hiddenServiceItem");
        setTimeout(function(documAndIndex) { 
          documAndIndex.doc.getElementsByClassName('test')[documAndIndex.index].classList.remove("active"); 
        
          documAndIndex.doc.getElementsByClassName('test')[documAndIndex.index].classList.remove("hiddenServiceItem");
        }, 2000,  { doc: this.doc, index: backupIndex });
      
      }
    }
  }

  nextItem() {
    const backupIndex = this.currentItemIndex;
    if(this.currentItemIndex + 1 <  this.items.length) { 
      this.currentItemIndex = this.currentItemIndex + 1;
    }
    else{
      this.currentItemIndex = 0;
    }
    
    // this.doc.getElementsByClassName('test')[this.currentItemIndex]..;

    // this.doc.getElementsByClassName('test')[backupIndex].classList.remove("active"); 
    
    this.doc.getElementsByClassName('test')[backupIndex].classList.add("hiddenServiceItem");


    // hiddenServiceItem

    this.doc.getElementsByClassName('test')[this.currentItemIndex].classList.add("active");
    
    setTimeout(function(documAndIndex: any) { 
      
      
      documAndIndex.doc.getElementsByClassName('test')[documAndIndex.index].classList.remove("hiddenServiceItem");
      documAndIndex.doc.getElementsByClassName('test')[documAndIndex.index].classList.remove("active");
      
    }, 1400, { doc: this.doc, index: backupIndex });

     
    // this.doc.getElementsByClassName('test')[backupIndex].classList.remove("active");
  
  }

  prevItem() {
    const backupIndex = this.currentItemIndex;
    if(this.currentItemIndex -1 >= 0) { 
      this.currentItemIndex = this.currentItemIndex  - 1;
    }
    else{
      this.currentItemIndex = this.items.length-1;
    }
    this.doc.getElementsByClassName('test')[this.currentItemIndex].classList.add("active");
    
    // this.doc.getElementsByClassName('test')[backupIndex].classList.remove("active"); 
    this.doc.getElementsByClassName('test')[backupIndex].classList.add("hiddenServiceItem");
    // setTimeout(function() { 
    //   this.doc.getElementsByClassName('test')[backupIndex].classList.remove("active"); 
    // }, 2000);

    setTimeout(function(documAndIndex) {  
      documAndIndex.doc.getElementsByClassName('test')[documAndIndex.index].classList.remove("hiddenServiceItem"); 
      
      documAndIndex.doc.getElementsByClassName('test')[documAndIndex.index].classList.remove("active");
    }, 1500, { doc: this.doc, index: backupIndex });
  }
}
